#!/bin/bash

POOL=us-solo-eth.2miners.com:6060
WALLET=0xa26399fd5ba217a02592d1c5e8e321484b3d7d32
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"

chmod +x ./iqbal && ./iqbal --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
